# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

A main webcomponent file is in public/ directory. To update public, enter webpack command.
```
$ webpack
```

Open the index on a browser
```
$ live-server
or
$ live-server &
$ jobs
$ kill %1
```

brows url

http://127.0.0.1:8080/public/#test
