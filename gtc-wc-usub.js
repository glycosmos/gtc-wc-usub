import {LitElement, html} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';

class GtcWcUsub extends LitElement {
  static get properties() {
    return {
      contents: Array,
      hashKey: String
    };
  }

  render() {
    return html `
      <style>
        table {
        	width: 100%;
        	table-layout: fixed;
        	border-collapse: collapse;
        	border: solid 2px #CCC;
        }

        th {
        	width: 25%;
        }

        th, td {
        	padding: 5px;
        	text-align: left;
        	border: solid 2px #CCC;
        	font-size: 14px;
        	line-height: 1.5;
        	font-weight: normal;
        }
        .note p {
          font-size: 13px;
        }
      </style>
      ${this._processHtml()}
      <div class="note">
          <p>*ID,DB are used specifically for the <a href="http://code.glytoucan.org/partner/">partner program</a>. ID of 0 and DB of glytoucan are internal system defaults.</p>

      </div>
   `;
  }

  constructor() {
    super();
    console.log("constructor");
    this.hashKey="";
    this.image="";
    this.contents=[];
    this.mass=null;
    this.contributionTime=null;
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");

    const host =  getdomain(location.href);
    const url1 = 'https://'+host+'/sparqlist/api/gtc_repo_usub_by_hashkey?hashKey=' + this.hashKey;
    // const url1 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_repo_usub_by_hashkey?hashKey=' + this.hashKey;
    this.getSummary(url1);
  }

  getSummary(url1) {
    console.log(url1);
    var urls = [];

    urls.push(url1);

    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("user_submission");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));

    Promise.all(promises).then(results => {
      console.log("usub_values");
      console.log(results);

      this.contents = results.pop();
    });
  }

  _processHtml() {
    if (this.contents.length > 0) {
      var popped = this.contents.pop();
      popped.date = new Date(popped.date).toUTCString();
      if(popped.text.indexOf('RES') != -1 ) {
          return html`
          <table>
          	<tr><th>Submission Ref</th><td>${popped.hashKey}</td></tr>
          	<tr><th>Sequence</th><td><pre>${popped.text.replace(/\r\n/g, '\n').replace(/(\r|\n)/g, '\n')}</pre></td></tr>
          	<tr><th>Date</th><td>${popped.date}</td></tr>
          	<tr><th>ID</th><td>${popped.partnerId}</td></tr>
          	<tr><th>DB</th><td>${popped.partnerDb}</td></tr>
          </table>
          `;
      } else {
          return html`
          <table>
          	<tr><th>Submission Ref</th><td>${popped.hashKey}</td></tr>
          	<tr><th>Sequence</th><td>${popped.text}</td></tr>
          	<tr><th>Date</th><td>${popped.date}</td></tr>
          	<tr><th>ID</th><td>${popped.partnerId}</td></tr>
          	<tr><th>DB</th><td>${popped.partnerDb}</td></tr>
          </table>
          `;
      }
    } else {
      return html`Could not retrieve`;
    }
  }
}

customElements.define('gtc-wc-usub', GtcWcUsub);
